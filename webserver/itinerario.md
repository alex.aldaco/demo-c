# Itinerario clase pico Web Server

0. Antecedentes de Redes
1. Función `main()` mínima
2. Explicación sobre sockets
	* `socket(2)`
	* `socket(7)`
	* File descriptor
3. Configuración de lado del server para escuchar
	* `ip(7)`
	* Uso de las funciones `htons(3)`, `htonl(3)` y `inet_aton(3)`
		* `htonl(3)` requiere de una constante
		* `inet_aton(3)` le pasamos un `string` que es una direccion de IP valida
4. Asignamos una dirección de memoria a la IP y puerto que configuramos
5. Escuchamos nuevas conexiones
	* `listen(2)`
