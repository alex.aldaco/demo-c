// Tipos datos comunes que suelen devolver las llamadas a las bibliotecas en sys
#include <sys/types.h>
// funcionalidad de socket
#include <sys/socket.h>
// funciones relacionadas con conexiones ip
#include <netinet/ip.h>
//#include <netinet/in.h>
// conversiones al byte order de la red
#include <arpa/inet.h>
// cerrar la conexion
#include <unistd.h>
// Entrada estandar
#include <stdio.h>

#define PORT 8080

int main(int argc, char const *argv[])
{
    // TODO: puerto y direccion como argumentos de linea de comandos
    // man 7 socket
    int sockfd = socket(AF_INET,    SOCK_STREAM,    0);
                      // ^ IPv4,    ^ Stream (TCP), ^ un solo protocolo en este socket
    // Manejo de error en la creación del socket
	if (sockfd == -1) {
        printf("Error abriendo socket\n");
        return 1;
    }

	// Configuraciones de IP para el server
	// struct para almacenar la info de la conexion IP
    struct sockaddr_in local_addr;
    // especificamos IPv4
    local_addr.sin_family = AF_INET;
	// man 3 htons
    local_addr.sin_port = htons(PORT); // asignamos el puerto
    // Asignamos la direccion (metodo 1)
	// man 3 htonl
    // local_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    // Asignamos la direccion (metodo 2)
	// man 3 inet_aton
    if (inet_aton("127.0.0.1", &local_addr.sin_addr) == 0) {
        printf("Error asignando la direccion 127.0.0.1 a la estructura\n");
        // limpiando
        close(sockfd);
        return 2;
    }

    // solicitamos al sistema operativo la direccion y puerto
    if (bind(sockfd, (struct sockaddr *)&local_addr, sizeof(local_addr)) == -1) {
        printf("Error asociando a la direccion %s puerto %d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
        // limpiando
        close(sockfd);
        return 3;
    }

    // escuchamos por nueva conexiones
	// man 2 listen
    if (listen(sockfd, 1) == -1) { // bloquea
        printf("Error al escuchar con el sockete\n");
        // limpiando
        close(sockfd);
        return 4;
    }

    // estructura para almacenar los datos del cliente
	// ver ip(7)
    struct sockaddr_in client_addr = {};
    unsigned int client_addr_len = sizeof(client_addr);
    int listen_sock;

    // creamos un nuevo socket para comunicarnos con el cliente
	// man 2 accept
    if ((listen_sock = accept(sockfd, (struct sockaddr *)&client_addr, &client_addr_len)) == -1) { // bloquea
        printf("Error estableciendo conexion con el cliente\n");
        return 5;
    }

    printf("Se establecio la conexion con %s en el puerto %d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

    // limpiando
    close(listen_sock);
    close(sockfd);

    return 0;
}
