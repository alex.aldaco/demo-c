#ifndef FIB_H
#define FIB_H

unsigned int fib_rec(unsigned int n);
void fib(unsigned int *buff, unsigned int n);

#endif // FIB_H