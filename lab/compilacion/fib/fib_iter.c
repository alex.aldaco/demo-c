#include "fib_iter.h"
#include <stdio.h>

void fib(unsigned int *buff, unsigned int n) {
    printf("Version iterativa\n");
    for (unsigned int i = 0; i < n; i++) {
        *(buff + i) = fib_iter(i);
    }
}

unsigned int fib_iter(unsigned int n) {
    unsigned int cur = 0, nxt = 1, tmp;
    for (unsigned int i = 0; i < n; i++)
    {
        tmp = cur;
        cur = nxt;
        nxt = tmp + cur;
    }
    return cur;
}
