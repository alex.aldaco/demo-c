// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

// Tipos de variables
#include <stdio.h>
#include <stdint.h>

int main(int argc, char const *argv[])
{
    // enteros

    int a, b;
    int c = 9;
    unsigned int d = 1;

    // reales

    float f = 32.0;
    double u = 11e3;

    // caracteres y cadenas

    char ch = 'c';
    char str[] = "Hola";

    printf("%s\n", str);

    // tipos mas razonables

    uint8_t i8 = 512;
    // uint8_t i8 = (uint8_t)512;
    printf("%d\n", i8);

    // reglas de conversion de C
    // procura no usarlas y hazlo de forma explicita

    // Constantes

    a  = 32.0f;
    //    ^ constante literal

    a = 32ll;
    //    ^ constante (long long)

    const float t = 0.3;
    // t = 0.4;
    // float *p = &t;

    #define MI_CONSTANTE 3.141592

    float pi = MI_CONSTANTE;

    return 0;
}
