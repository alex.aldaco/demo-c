// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

#include <stdio.h>
#include <string.h>

// Tipo de dato compuesto básico de C, un struct representa un bloque
// contiguo de memoria que almacena variables relacionadas entre si
struct book {
    int id;
    char title[12];
    char author[13];
    float price;
};

// Forma alternativa de declarar un struct, de esta manera se crea un
// alias para este struct, ahora nos podemos referir a esta estructura
// como Book en el código
// typedef struct {
//     int id;
//     char title[12];
//     char author[12];
//     float price;
// } Book;

// typedef se puede usar con cualquier tipo de dato
// por ejemplo podemos renombrar `unsigned char` como `u8`
// typedef unsigned char u8

int main()
{
    // Esta es la forma de declarar una nueva variable de tipo struct
    struct book my_book;
    // Book my_book; // esta es usando la segunda forma con typedef
    my_book.id    = 0;
    my_book.price = 100.0f;

    // Tambien podemos hacerlo de esta forma
    // struct book my_book = {.id = 0, .price = 100.0f};
    // TODO: como lo harias usando la declaracion alternativa con typedef?
    // _______________ = {.id = 0, .price = 100.0f};

    // ! No podemos asignar constantes de cadena a arreglos, debemos copiar elemento a elemento
    // ! my_book.title = "Hola mundo";  // Error

    strncpy(my_book.title, "Hola mundo", 12);
    // ! Nota la diferencia entre strcpy y strncpy, la primera no sabe si la
    // ! memoria donde esta guardando la cadena es del tamaño adecuado o esta
    // ! sobreescribiendo memoria que no deberia tocar
    // strcpy(my_book.author, "Hola Mundo");
    // ! strncpy solo almacena hasta n (tercer argumento) caracteres, esto
    // ! no evita que sobreescriba memoria que no deberia tocar, pero obliga al
    // ! programador a ser más consciente del tamaño de sus apuntadores/arreglos
    strncpy(my_book.author, "Diego Barrigadsd", 12);

    printf("id,\ttitle,\tauthor,\tprice\n");
    printf("%d, %s, %s, %f\n", my_book.id, my_book.title, my_book.author, my_book.price);


    printf("tamano en memoria de struct book: %u\n", sizeof(struct book));
    printf("direccion de my_book:        %u\n", &my_book);         // imprimimos la direccion de memoria donde se encuentra my_book
    printf("direccion de my_book.id:     %u\n", &my_book.id);      // my_book y my_book.id tienen la misma direccion, por qué?
    printf("direccion de my_book.title:  %u\n", &my_book.title);   // cuantos bytes separan a 
    printf("direccion de my_book.author: %u\n", &my_book.author);  // title y author?
    printf("direccion de my_book.price:  %u\n", &my_book.price);   // cuantos bytes separan a author y price?

    // TODO: cambia la longitud de author de 12 elementos a 13 elementos
    // ? que tamañao ocupa en memoria la estructura ahora? por que?
    // ? cuantos bytes separan a author de price?

    return 0;
}
