// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

#include <stdio.h>
#include <stdlib.h>

// Las uniones son un tipo que permite almacenar variables de distinto tipo
// en un mismo espacio de memoria.
union id
{
    int my_id;    // 4 bytes
    char doi[20]; // 20 bytes
    int isbn[10]; // 40 bytes
};
// El tamaño final de las uniones es igual al del miembro de mayor tamaño
// cuando se declara la union. En este caso una variable de tipo `union id`
// ocupará 40 bytes en memoria.

// Un patron comun en C es usar enums asosciados a las uniones para 
// determinar el tipo de dato que esta albergando cada instancia de una union
typedef enum
{
    Novel, // id -> usar union como si fuera un int
    Magazine, // isbn -> usar union como si fuera un int[10]
    Cookbook, // id -> usar union como si fuera un int
    Dictionary, // doi -> usar union como si fuera un char[20]
} BookType;

// despues de definir una union y un enum asociado podemos almacenar
// ambos en una estructura para siempre tener a la mano la union y su tag
struct book
{
    union id id;
    char title[12];
    char author[12];
    // enum book_type type;
    BookType type;
    float price;
};

void print_book(struct book mybook)
{
    // antes de imprimir algun libro, debemos determinar como
    // acceder a la informacion dentro de `id`
    if (mybook.type == Magazine) {
        for (int i = 0; i < 10; i++)
        {
            printf("%d ", mybook.id.isbn[i]);
        }
    } else if (mybook.type == Dictionary) {
        printf("%s", mybook.id.doi);
    } else {
        printf("%d", mybook.id.my_id);
    }
    // lo mismo para cualquier tipo de libro
    printf(", %s, %s, %d, %f\n", mybook.title, mybook.author, mybook.type, mybook.price);
}

void print_books(struct book my_books[], unsigned int n) {
    printf("id,\ttitle,\tauthor,\ttype,\tprice\n");
    for (unsigned int i = 0; i < n; i++)
    {
        print_book(my_books[i]);
    }

}

int main()
{
    // setup seed
    srand(33);

    // Book library[10];
    struct book library[10];

    for (int i = 0; i < 10; i++)
    {
        library[i].price = 100.0f;
        library[i].type = rand() % 4;
        for (int j = 0; j < 10; j++)
        {
            library[i].title[j] = 97 + rand() % 26;
            library[i].author[j] = 97 + rand() % 26;
        }
        library[i].title[10] = '\0';
        library[i].author[10] = '\0';
    }

    print_books(library, 10);

    return 0;
}
